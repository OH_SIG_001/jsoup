# jsoup

## Introduction

jsoup is a library that offers fast and easy-to-use HTML parsing. Below describes its functions:

- Captures and parses HTML docs from URLs, files, or strings.
- Converts HTML documents into the DOM structure and extracts attributes and texts from elements.
- Manipulates HTML elements, attributes, and texts.
- Cleans user-submitted HTML content against a white-list.
- Outputs data in HTML or XHTML format.


## How to Install

Perform the following operations by function:

Scenario 1: Parse, extract , and clean HTML documents.

```
ohpm install @ohos/sanitize-html 
```

Scenario 2: Converts HTML to XHTML.

```
ohpm install @ohos/htmltoxml
```

Scenario 3: Converts HTML to JSON.

```
ohpm install parser-html-json
```

For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md) to us.

## How to Use

### HTML operations

#### Parsing HTML docs and extracting attributes and text from elements

- Configure **GlobalContext** in **src/main/ets/entryability/EntryAbility.ts**.

```
  GlobalContext.getContext().setValue("resManager", this.context.resourceManager);
  GlobalContext.getContext().setValue("filesPath", this.context.filesDir);
  GlobalContext.getContext().setValue("context", this.context);
```

- Create **Partial<Handler>,(helper.ts)**.

```
import type { Parser } from "htmlparser2";
import { Handler } from 'htmlparser2/src/main/ets/esm/Parser';

interface Event {
    $event: string;
    data: unknown[];
    startIndex: number;
    endIndex: number;
}

/**
 * Creates a handler that calls the supplied callback with simplified events on
 * completion.
 *
 * @internal
 * @param callback Function to call with all events.
 */
export function getEventCollector(
    callback: (error: Error | null, data?: ESObject) => void,
): Partial<Handler> {
    const events: Event[] = [];
    let parser: Parser;

    function handle(event: string, data: unknown[]): void {
        switch (event) {
            case "onerror": {
                callback(data[0] as Error);
                break;
            }
            case "onend": {
                callback(null, {
                    $event: event.slice(2),
                    startIndex: parser.startIndex,
                    endIndex: parser.endIndex,
                    data,
                });
                break;
            }
            case "onreset": {
                events.length = 0;
                break;
            }
            case "onparserinit": {
                parser = data[0] as Parser;
                break;
            }

            case "onopentag": {
                callback(null, {
                    $event: event.slice(2),
                    startIndex: parser.startIndex,
                    endIndex: parser.endIndex,
                    data,
                });
                break;
            }

            case "ontext": {
                callback(null, {
                    $event: event.slice(2),
                    startIndex: parser.startIndex,
                    endIndex: parser.endIndex,
                    data: data[0],
                })
                break;
            }

            case "onclosetag": {
                if (data[0] === "script") {
                    console.info("htmlparser2--That's it?!");
                }
                break;
            }
            default: {
                const last = events[events.length - 1];
                if (event === "ontext" && last && last.$event === "text") {
                    (last.data[0] as string) += data[0];
                    last.endIndex = parser.endIndex;
                    break;
                }

                if (event === "onattribute" && data[2] === undefined) {
                    data.pop();
                }

                if (!(parser.startIndex <= parser.endIndex)) {
                    throw new Error(
                        `Invalid start/end index ${parser.startIndex} > ${parser.endIndex}`,
                    );
                }

                events.push({
                    $event: event.slice(2),
                    startIndex: parser.startIndex,
                    endIndex: parser.endIndex,
                    data,
                });
                parser.endIndex;
            }
        }
    }

    return new Proxy(
        {},
        {
            get:
            (_, event: string) =>
            (...data: unknown[]) =>
            handle(event, data),
        },
    );
}
```

- Build a parser using the handler.

```
import { Parser } from 'htmlparser2'

let  parser = new Parser(helper.getEventCollector((error, actual: ESObject) => {
    if (actual.$event == "opentag") {
        this.addLog(this.parserContent, `jsoup-- onopentag name --> ${actual.data[0]}  attributes --> ${JSON.stringify(actual.data[1])}`);
    }
    if (actual.$event == "text") {
        this.addLog(this.parserContent, "jsoup-- text -->" + actual.data);
    }
    if (actual.$event == "opentagname") {
        this.addLog(this.parserContent, "jsoup-- tagName -->" + actual.data);
    }
    if (actual.$event == "attribute") {
        this.addLog(this.parserContent, `jsoup-- attribName name --> ${actual.data[0]}  value --> ${actual.data[1]}`);
    }
    if (actual.$event == "closetag") {
        this.addLog(this.parserContent, "jsoup-- closeTag --> " + actual.data);
    }
    if (actual.$event == "end") {
        this.showResult(this.parserContent.join('\n'))
        this.parserContent = [];
    }
}));
parser.write(html);
parser.end();
```

- Build a parser using the DomHandler.

```
import { Parser } from 'htmlparser2'
import { DomHandler } from 'domhandler'
import * as DomUtils from 'domutils'

const handler = new DomHandler((error, dom) => {
  if (error) {
    // Handle error
  } else {
    // Parsing completed, do something
    console.info('jsoup dom.toString()=' + dom + "");
    let elements = DomUtils.getElementsByTagName('style', dom)
    console.info('jsoup elements.length=', elements.length);
    let element = elements[0]
    console.info('jsoup element=', Object.keys(element));
    let text = DomUtils.getText(elements)
    console.info('jsoup text=', text); 
  }
});
const parser = new Parser(handler, { decodeEntities: true });
parser.write(html);
parser.end();
```

- Parse **parseDocument**.

```
import { parseDocument } from 'htmlparser2'
import * as DomUtils from 'domutils'

let dom: Document = parseDocument(html)
// Use DomUtils to perform operations on the parsed DOM object.
// Obtain the element based on the tag name.
let element = DomUtils.getElementsByTagName('style', dom)
// Obtain the text.
let text = DomUtils.getText(element)
// Check whether the element type is **tag**.
let isTag = DomUtils.isTag(element[0])
// Check whether the element type is **CDATA**.
let isCDATA = DomUtils.isCDATA(element[0])
// Check whether the element type is **Text**.
let isText = DomUtils.isText(element[0])
// Check whether the element type is **Comment**.
let isComment = DomUtils.isComment(element[0])
// Obtain the sub-element set of a specified element.
let childrens = DomUtils.getChildren(body[0])
```

#### Obtaining the HTML text

- Obtaining HTML texts by URL

```
import http from '@ohos.net.http';

let httpRequest = http.createHttp()
httpRequest.request('http://106.15.92.248/share/html.txt')
  .then((data) => {
    console.log("jsoup url html=" + JSON.stringify(data))
   	// TODO do something
    if (data.result && typeof data.result === 'string') {
      parser.write(data.result);
      parser.end();
    }
  })
  .catch((err) => {
    console.error('jsoup connect error:' + JSON.stringify(err));
  })
```

- Obtaining HTML texts by file streams.

```
import fileio from '@ohos.fileio';

let buf = new ArrayBuffer(html.length)
stream.readSync(buf, {
  offset: 0, length: html.length, position: 0
})
let dom = String.fromCharCode.apply(null, new Uint8Array(buf))
// TODO  do something
parser.write(dom);
parser.end();
```

- Obtains HTML texts using rawfile.

```
import util from '@ohos.util';

// Note: You need to assign a value to the variable in the MainAbility.
let resourceManager=GlobalContext.getContext().getValue("resManager") as resmgr.ResourceManager
if (!resourceManager ) {
  console.log('jsoup resourceManager is undefined');
  return;
}
resourceManager.getRawFile(filePath)
  .then((data) => {
    var textDecoder = new util.TextDecoder("utf-8", {
      ignoreBOM: true
    })
    var result: string = textDecoder.decode(data, {
      stream: false
    })
    // TODO do something
    parser.write(result);
    parser.end();
  })
  .catch((err) => {
    console.log("jsoup getHtmlFromRawFile err=" + err)
  })
```

- Obtain HTML texts by file path.

```
 import fileio from '@ohos.fileio';
 
 let filesPath = GlobalContext.getContext()
            .getValue("filesPath") as string
 
 if (!filesPath) {
   console.log('jsoup filesPath is undefined');
   return;
 }
 var filePath = filesPath + '/jsoup.html';
 fileio.readText(filePath)
   .then((data) => {
     console.log("jsoup getHtmlFromFilePath text=" + data);
     // TODO do something
     parser.write(data);
     parser.end();
   })
   .catch((err) => {
     console.log("jsoup getHtmlFromFilePath err=" + err)
   })
```


#### Cleaning HTML and Operating HTML Elements, Attributes, and Texts

- Import a module.
 ```
import SanitizeHtml from 'sanitize-html'
 ```
- Cleans HTML texts.

Use the default tag and attribute list.
```
const clean = SanitizeHtml(dirty);
```
Allowed tags and attributes are not cleared.

 ```
 const clean = sanitizeHtml(dirty, {
  allowedTags: [ 'b', 'i', 'em', 'strong', 'a' ],
  allowedAttributes: {
    'a': [ 'href' ]
  },
  allowedIframeHostnames: ['www.youtube.com']
});
 ```

Add tags to the default list.
```
const clean = SanitizeHtml(dirty, {
  allowedTags: SanitizeHtml.defaults.allowedTags.concat([ 'img' ])
});
```

Escape unallowed tags instead of clearing them.
 ```
const clean = SanitizeHtml('before <img src="test.png" /> after', {
  disallowedTagsMode: 'escape',
  allowedTags: [],
  allowedAttributes: false
 })
 ```
Allow all tags or all attributes.
```
allowedTags: false,
allowedAttributes: false
```

Do not allow any tags.
```
allowedTags: [],
allowedAttributes: {}
```
Allow a specific CSS class on a specific element.
```
const clean = SanitizeHtml(dirty, {
allowedTags: [ 'p', 'em', 'strong' ],
allowedClasses: {
'p': [ 'fancy', 'simple' ]
}
});
```
Allow a specific CSS style on a specific element.
```
const clean = SanitizeHtml(dirty, {
        allowedTags: ['p'],
        allowedAttributes: {
          'p': ["style"],
        },
        allowedStyles: {
          '*': {
            // Match HEX and RGB
            'color': [/^#(0x)?[0-9a-f]+$/i, /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/],
            'text-align': [/^left$/, /^right$/, /^center$/],
            // Match any number with px, em, or %
            'font-size': [/^\d+(?:px|em|%)$/]
          },
          'p': {
            'font-size': [/^\d+rem$/]
          }
        }
      });
```

- Change tags.

 ```
 const dirty='<ol><li>Hello world</li></ol>';
 const clean = SanitizeHtml(dirty, {
  transformTags: {
    'ol': 'ul',
  }
});
 ```

Change the tag and add attributes.
```
const dirty = '<ol foo="foo" bar="bar" baz="baz"><li>Hello world</li></ol>';
const clean = SanitizeHtml(dirty, {
              transformTags: { ol: SanitizeHtml.simpleTransform('ul', { class: 'foo' }) },
              allowedAttributes: { ul: ['foo', 'bar', 'class'] }
            });
```

- Add or modify the text content of a tag.

```
const clean = SanitizeHtml(dirty, {
  transformTags: {
    'a': function(tagName, attribs) {
      return {
        tagName: 'a',
        attribs: attribs,
        text: 'Some text'
      };
    }
  }
});
```

For example, you can convert a link element that lacks anchor text to a link element with anchor text as follows:

```
<a href="http://somelink.com"></a>
```

After conversion:

```
<a href="http://somelink.com">Some text</a>
```

- Delete unnecessary tags using the filtering function.

```
const dirty = '<p>This is <a href="http://www.linux.org"></a><br/>Linux</p>';
const clean = SanitizeHtml(dirty, {
              exclusiveFilter: function (frame) {
                return frame.tag === 'a' && !frame.text.trim();
              }
            });
  ```

### Converting HTML to Clean XHTML Texts

 ```
import { XMLWriter } from '@ohos/htmltoxml'
let property = [{ key: XMLWriter.DOCTYPE_PUBLIC, value: '-//W3C//DTD XHTML 1.1//EN' },
{ key: XMLWriter.DOCTYPE_SYSTEM, value: 'http://www.w3.org/TR?xhtml11/DTD/xhtml11.dtd' }]
const xml = new XMLWriter(html, property);
xml.convertToXML((content, error) => {

})
 ```

 ### Extract CSS.

 ```
import * as ParserHTMLJson from 'parser-html-json'

let parserJson = new ParserHTMLJson.default(html);
let result = parserJson.getClassStyleJson();
console.info("jsoup css=" + JSON.stringify(result));
 ```

## Available APIs

Define types.

```
 // Parser processing callback
 interface Handler {
   onparserinit(parser: Parser): void;
   onreset(): void;
   onend(): void;
   onerror(error: Error): void;
   onclosetag(name: string): void;
   onopentagname(name: string): void;
   onattribute(name: string, value: string, quote?: string | undefined | null): void;
   onopentag(name: string, attribs: {
       [s: string]: string;
   }): void;
   ontext(data: string): void;
   oncomment(data: string): void;
   oncdatastart(): void;
   oncdataend(): void;
   oncommentend(): void;
   onprocessinginstruction(name: string, data: string): void;
 }

// Parser options
 interface ParserOptions {
   decodeEntities?: boolean;
   lowerCaseTags?: boolean;
   lowerCaseAttributeNames?: boolean;
   recognizeCDATA?: boolean;
 }
 
 // Clean HTML to defend against XSS attacks.
 declare namespace sanitize {
  interface Attributes { [attr: string]: string; }

  interface Tag { tagName: string; attribs: Attributes; text?: string ; }

  type Transformer = (tagName: string, attribs: Attributes) => Tag;

  type AllowedAttribute = string | { name: string; multiple?: boolean ; values: string[] };

  type DisallowedTagsModes = 'discard' | 'escape' | 'recursiveEscape';

  interface IDefaults {
    allowedAttributes: Record<string, AllowedAttribute[]>;
    allowedSchemes: string[];
    allowedSchemesByTag: { [index: string]: string[] };
    allowedSchemesAppliedToAttributes: string[];
    allowedTags: string[];
    allowProtocolRelative: boolean;
    disallowedTagsMode: DisallowedTagsModes;
    enforceHtmlBoundary: boolean;
    selfClosing: string[];
  }

  interface IFrame {
    tag: string;
    attribs: { [index: string]: string };
    text: string;
    tagPosition: number;
  }

  interface IOptions {
    allowedAttributes?: Record<string, AllowedAttribute[]> | false;
    allowedStyles?: { [index: string]: { [index: string]: RegExp[] } } ;
    allowedClasses?: { [index: string]: boolean | Array<string | RegExp> };
    allowIframeRelativeUrls?: boolean ;
    allowedSchemes?: string[] | boolean ;
    allowedSchemesByTag?: { [index: string]: string[] } | boolean ;
    allowedSchemesAppliedToAttributes?: string[] ;
    allowProtocolRelative?: boolean ;
    allowedTags?: string[] | false ;
    allowVulnerableTags?: boolean ;
    textFilter?: ((text: string, tagName: string) => string) ;
    exclusiveFilter?: ((frame: IFrame) => boolean) ;
    nonTextTags?: string[] ;
    selfClosing?: string[] ;
    transformTags?: { [tagName: string]: string | Transformer } ;
    parser?: ParserOptions ;
    disallowedTagsMode?: DisallowedTagsModes ;
    enforceHtmlBoundary?: boolean ;
  }

  const defaults: IDefaults;
  const options: IOptions;

  function simpleTransform(tagName: string, attribs: Attributes, merge?: boolean): Transformer;
  }
```

API definition

| API                                                      | Parameter                    | Description                                                |
| ------------------------------------------------------------ | ------------------------ | -------------------------------------------------------- |
| new Parser(cbs: Partial\<Handler\>  &#124; null, options?: ParserOptions) | handler,ParserOptions    | Creates an HTML parser.                                          |
| write(chunk: string): void                                | string                   | Writes data to the HTML parser, parses a large block of data, and calls the corresponding callback.|
| end(chunk?: string): void                                   | string                   | Parses the end of the buffer and clears the stack by calling **onend**.                |
| parseComplete(data: string): void                           | string                   | Resets the parser, and then parses the complete document and pushes it to the handler.    |
| parseDocument(data: string, options?: ParserOptions): Document | string,ParserOptions     | Parses data and returns the result file.                                |
| SanitizeHtml(dirty: string, options?: sanitize.IOptions): string | string,sanitize.IOptions | Cleans HTML to implement HTML trustworthiness.                                |
|new XMLWriter(html: string, property?: Array\<option\>)          |  string,Array\<option\>   | Creates an XHTML converter object.     |
|convertToXML(callback: (content: string &#124; null, error?: Error) => void):void | callback  | Converts HTML to XHTML.|
| new ParserHTMLJson.default(html: string) | html    | Creates an HTML JSON parser.
| getClassStyleJson() |  N/A  | Extracts CSS.
| getHtmlJson() |   N/A | Obtains the HTML string in JSON format.


For details about the DomUtils interface definition, see [DomUtils](https://domutils.js.org/modules.html).

##  Constraints
This project has been verified in the following version:

DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)

## Directory Structure

````
|---- jsoup  
|     |---- entry  # Sample code
|        |----src/main/ets
|            |pages
|                |----addTag.ets
|                |----index.ets
|                |----showResult.ets
| |---- library # Convert HTML into XHTMl.
|     |---- README.md  # Readme
|     |---- README_zh.md  # Readme
````

## How to Contribute

If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/jsoup/issues) or a [PR](https://gitee.com/openharmony-sig/jsoup/pulls).

## License

This project is licensed under [MIT License](https://gitee.com/openharmony-sig/jsoup/blob/master/LICENSE).
